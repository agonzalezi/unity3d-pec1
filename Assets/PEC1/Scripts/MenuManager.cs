﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Slider sliderRepetition;
    public Button repetitionButton;

    private void Start()
    {
        // Verifica si existe informacion de la repeticion para activar la opcion
        CheckReplay();
    }

    // Inicia el juego en modo carrera
    public void StartRun()
    {
        GameStats.State = GameStats.GameMode.run; 
        SceneManager.LoadScene("Game");
    }

    // Inicia el juego en modo repeticion y en base al slider, se activara el modo repeticion normal o cinematica
    public void StartRepetition()
    {
        if (repetitionButton.interactable)
        {
            if (sliderRepetition.value == 0)
            {
                GameStats.State = GameStats.GameMode.repetititon;
            }
            else
            {
                GameStats.State = GameStats.GameMode.cinematicrepetition;
            }
            SceneManager.LoadScene("Game");
        }
        
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    // Regresa al menu
    public void ReturnMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    // Verifica si existe informacion del replay para activar el boton de repeticion
    public void CheckReplay()
    {
        TextAsset targetFile = Resources.Load<TextAsset>("replayInfo");
        if (targetFile==null)
        {
            sliderRepetition.interactable = false;
            repetitionButton.interactable = false;
        }
        else
        {
            sliderRepetition.interactable = true;
            repetitionButton.interactable = true;
        }
    }
}
