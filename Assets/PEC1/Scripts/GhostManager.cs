﻿using UnityEngine;
using UnityStandardAssets.Utility;
using static UnityStandardAssets.Utility.WaypointCircuit;

public class GhostManager : MonoBehaviour
{
    // Circuito y tracker de la IA
    public WaypointCircuit circuito;
    public WaypointProgressTracker tracker;

    // Conjunto de coches
    public GameObject car;
    public GameObject ghostCar;
    public GameObject replayCar;

    // Guardamos el fantasma de cada vuelta
    public GhostPositions ghostTransformsList = new GhostPositions();

    //Guardamos todas las vueltas
    public GhostPositions replayTransformsList = new GhostPositions();
    public float timeTrack;

    public GhostPositionsManager managerPositions;

    public bool saved = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Si no se ha guardado el fantasma se captura cada posicion y rotacion del jugador
        if (!saved)
        {
            timeTrack += Time.deltaTime;

            GhostPosition ghostposition = new GhostPosition();
            ghostposition.positionx = car.transform.position.x;
            ghostposition.positiony = car.transform.position.y;
            ghostposition.positionz = car.transform.position.z;

            ghostposition.rotationx = car.transform.rotation.x;
            ghostposition.rotationy = car.transform.rotation.y;
            ghostposition.rotationz = car.transform.rotation.z;
            ghostposition.rotationw = car.transform.rotation.w;

            // Solo en el caso de que sean diferentes al punto anterior se guardaran
            if(ghostTransformsList.ghostPositionCollection.Count==0||!Comparepoints(ghostTransformsList.ghostPositionCollection[ghostTransformsList.ghostPositionCollection.Count-1],ghostposition))
            {
                // Se guardan tanto la vuelta actual para el fantasma, como todas para la repeticion
                ghostTransformsList.ghostPositionCollection.Add(ghostposition);
                replayTransformsList.ghostPositionCollection.Add(ghostposition);
            }

        }
        else
        {
            // Iniciamos un nuevo fantasma
            saved = false;
            timeTrack = 0f;
            ghostTransformsList.ghostPositionCollection.Clear();
            ghostTransformsList.totalTime = 0f;
        }
        
    }

    // Guardamos la informacion del fantasma
    public void SaveGhost()
    {
        // Si el jugador, ha conseguido mejorar la marca del fantasma, se guarda la información del nuevo fantasma
        if ((managerPositions.ghostPositionsSaved!=null&& managerPositions.ghostPositionsSaved.totalTime > timeTrack) || managerPositions.ghostPositionsSaved == null || managerPositions.ghostPositionsSaved.totalTime == 0f)
        {
            ghostTransformsList.totalTime = timeTrack;
            managerPositions.SaveGhostPositions(ghostTransformsList);
        }
        saved = true;
    }

    // Se guarda la informacion de todas las vueltas para la repeticion
    public void SaveReplay()
    {
        managerPositions.SaveReplayPositions(replayTransformsList);
    }

    // Cargamos del fichero JSON las posiciones para las repeticiones
    public void LoadReplay()
    {
        // Cargamos el fichero
        managerPositions.LoadReplayPositions();

        //Asignamos a la carretera
        AssignWaypoints(managerPositions.replayPositionsSaved);

        // Se inicia el coche fantasma en la primera posicion y rotacion, con una diferencia de 1
        replayCar.gameObject.transform.position = new Vector3(managerPositions.replayPositionsSaved.ghostPositionCollection[0].positionx - 1, managerPositions.replayPositionsSaved.ghostPositionCollection[0].positiony, managerPositions.replayPositionsSaved.ghostPositionCollection[0].positionz);
        replayCar.gameObject.transform.rotation = new Quaternion(managerPositions.replayPositionsSaved.ghostPositionCollection[0].rotationx, managerPositions.replayPositionsSaved.ghostPositionCollection[0].rotationy, managerPositions.replayPositionsSaved.ghostPositionCollection[0].rotationz, managerPositions.replayPositionsSaved.ghostPositionCollection[0].rotationw);
        //Reiniciamos los puntos del circuito, para que obtenga los cambios del nuevo circuito, en el caso de que se haya cambiado
        circuito.initializeIACircuit();
        // Para que en la segunda vuelta que el fantasma tenga que dar no continue por el punto target por donde iba
        tracker.resetWaypointProgressTracker();
    }

    // En caso de que exista fantasma, se cargan sus posiciones y se activa
    public void LoadGhost()
    {
        // Si el jugador, ha conseguido mejorar la marca del fantasma, se guarda la información del nuevo fantasma
        if (managerPositions.ghostPositionsSaved.ghostPositionCollection.Count>0)
        {
            Debug.Log("HAY FANTASMA");

            AssignWaypoints(managerPositions.ghostPositionsSaved);

            //Se pone en la posicion inicial el fantasma
            ghostCar.SetActive(false);
            // Se inicia el coche fantasma en la primera posicion y rotacion, con una diferencia de 1
            ghostCar.gameObject.transform.position = new Vector3(managerPositions.ghostPositionsSaved.ghostPositionCollection[0].positionx-1, managerPositions.ghostPositionsSaved.ghostPositionCollection[0].positiony, managerPositions.ghostPositionsSaved.ghostPositionCollection[0].positionz);
            ghostCar.gameObject.transform.rotation = new Quaternion(managerPositions.ghostPositionsSaved.ghostPositionCollection[0].rotationx, managerPositions.ghostPositionsSaved.ghostPositionCollection[0].rotationy, managerPositions.ghostPositionsSaved.ghostPositionCollection[0].rotationz, managerPositions.ghostPositionsSaved.ghostPositionCollection[0].rotationw);
            //Reiniciamos los puntos del circuito
            circuito.initializeIACircuit();
            tracker.resetWaypointProgressTracker();
            ghostCar.SetActive(true);
        }    
    }

    // Se pasa del objeto GhostPositions a una lista de transforms para poder asignarlo al circuito
    public void AssignWaypoints(GhostPositions positionList)
    {
        // Creamos una lista de waypoints
        WaypointList waypointList = new WaypointList();
        waypointList.circuit = circuito;
        Transform[] items = new Transform[positionList.ghostPositionCollection.Count];

        int i = 0;
        foreach (GhostPosition ghostposition in positionList.ghostPositionCollection)
        {
            GameObject gameobj = new GameObject();
            Quaternion rotation = new Quaternion(ghostposition.rotationx, ghostposition.rotationy, ghostposition.rotationz, ghostposition.rotationw);
            Vector3 position = new Vector3(ghostposition.positionx, ghostposition.positiony, ghostposition.positionz);
            
            gameobj.transform.rotation = rotation;
            gameobj.transform.position = position;
            items[i] = gameobj.transform;

            i++;
        }

        waypointList.items = items;

        circuito.waypointList = waypointList;
    }


    // Compara dos puntos, para verificar si son iguales o no
    private bool Comparepoints(GhostPosition pos1, GhostPosition pos2)
    {
        if (pos1.positionx != pos2.positionx) return false;
        if (pos1.positiony != pos2.positiony) return false;
        if (pos1.positionz != pos2.positionz) return false;

        if (pos1.rotationx != pos2.rotationx) return false;
        if (pos1.rotationy != pos2.rotationy) return false;
        if (pos1.rotationz != pos2.rotationz) return false;
        if (pos1.rotationw != pos2.rotationw) return false;
        return true;
    }
}
