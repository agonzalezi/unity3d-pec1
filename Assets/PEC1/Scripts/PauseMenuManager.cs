﻿using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject menu;

    // Update is called once per frame
    void Update()
    {
        // Si pulsa espacio, aparecen los dos botones que permiten volver al menu o salir
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (menu.gameObject.activeInHierarchy == true)
            {
                menu.gameObject.SetActive(false);
            }
            else
            {
                menu.gameObject.SetActive(true);
            }
        }
    }
}
