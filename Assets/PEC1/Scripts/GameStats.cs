﻿using UnityEngine;

public static class GameStats
{
    // Indica la vuelta actual del coche
    private static int actualLap = 0;
    // Indica si el juego se ha ganado
    private static bool win = false;
    // Indicador de que se ha cambiado de vuelta, para realizar el guardado del fantasma y cargarlo
    private static bool changedLap = false;

    public static int ActualLap { get => actualLap; set => actualLap = value; }
    public static bool Win { get => win; set => win = value; }
    public static GameMode State { get => state; set => state = value; }
    public static bool ChangedLap { get => changedLap; set => changedLap = value; }

    // Modos de juego, carrera, repeticion normal y repeticion cinematica
    public enum GameMode
    {
        run,
        repetititon,
        cinematicrepetition
    }
    private static GameMode state = GameMode.run;

    // Metodo de cambio de vuelta, en base a si se ha cambiado de vuelta y la vuelta actual, se detecta si se ha ganado el juego
    public static void ChangeLap()
    {
        Debug.Log("ACTUAL LAP "+ actualLap);
        if (actualLap!=2 && !ChangedLap)
        {
            ChangedLap = true;

            actualLap++;
        }
        else
        {
            Debug.Log("WIN");
            Win = true;
            
        }
    }

    // Reset del juego
    public static void ResetGame()
    {
        actualLap = 0;
        win = false;
        changedLap = false;
    }
}
