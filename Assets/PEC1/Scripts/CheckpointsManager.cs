﻿using UnityEngine;

public class CheckpointsManager : MonoBehaviour
{
    // Lista de los checkpoints
    public Transform[] checkpointList;

    private bool lapDone = false;

    public Transform camerasContainer;
    public Transform[] cameras;

    private int actualCamera = 1;
    private int numberOfCheckpoints = 8;
    
    void Start()
    {
        //Obtenemos la lista de checkpoint
        checkpointList = transform.GetComponentsInChildren<Transform>();

        //Obtenemos la lista de camaras
        if (GameStats.State == GameStats.GameMode.cinematicrepetition)
        {
            cameras = camerasContainer.GetComponentsInChildren<Transform>(true);
        }

        //Desactivamos los checkpoints para que no se vean
        foreach (Transform checkpoint in checkpointList)
        {
            checkpoint.gameObject.SetActive(false);
        }

        // Activamos el primero al iniciar la carrera
        checkpointList[0].gameObject.SetActive(true);
        // Activamos el padre
        checkpointList[1].gameObject.SetActive(true);
    }

    // En caso de que haya una colision algun checkpoint, se activara el sonido y se cambiara de checkpoint activo
    public void CollisionOnChild(Transform child, Collider other)
    {
        if (other.gameObject.CompareTag("PlayerColliders"))
        {
            // Sonido de chocar con un checkpoint
            transform.GetComponents<AudioSource>()[0].Play();
            ChangeCheckpointOnCollision(child.gameObject);
            // En el caso de que se colisione con el checkpoint en metodo cinematica, se cambia de camara
            if (GameStats.State == GameStats.GameMode.cinematicrepetition)
            {
                ChangeCameraOnCollision();
            }
        }
    }

    // Desactiva el checkpoint tocado y activa el siguiente checkpoint
    private void ChangeCheckpointOnCollision(GameObject checkpoint)
    {
        // Se obtiene numero de checkpoint en base al nombre del objeto
        string checkpointNumber = checkpoint.name.Substring(checkpoint.name.Length-1, 1);
        
        // Desactivamos el checkpoint actual
        checkpointList[int.Parse(checkpointNumber)].gameObject.SetActive(false);
        if (int.Parse(checkpointNumber) == 1 && lapDone)
        {
            // En caso de ser completar la vuelta y pasar por el checkpoint 1, cambiamos de vuelta.
            lapDone = false;
            // Cambiamos de vuelta
            GameStats.ChangeLap();
        }
        // Se detecta de que se ha dado una vuelta completa y se activa el primero
        if (int.Parse(checkpointNumber)== numberOfCheckpoints)
        {
            lapDone = true;
            checkpointList[1].gameObject.SetActive(true);
        }
        else
        {
            // Activamos el siguiente
            checkpointList[int.Parse(checkpointNumber) + 1].gameObject.SetActive(true);
        }

        
    }

    // Cambia de camara cuando el coche choca con un checkpoint
    private void ChangeCameraOnCollision()
    {
        // Desactivamos la ultima camara si se ha dado una vuelta
        if (actualCamera == 1)
        {
            cameras[9].gameObject.SetActive(false);
        }
        actualCamera++;
        
        // Activamos la camara siguiente y desactivamos la anterior
        cameras[actualCamera].gameObject.SetActive(true);
        cameras[actualCamera-1].gameObject.SetActive(false);

        if (actualCamera==9)
        {
            actualCamera = 1;
        }
    }
}
