﻿using UnityEngine;

public class CheckpointCollisionManager : MonoBehaviour
{
    // Cuando existe una colision en un checkpoint, llama a la funcion de colision en hijo, en el padre
    private void OnTriggerEnter(Collider other)
    {
        transform.parent.GetComponent<CheckpointsManager>().CollisionOnChild(transform, other);
    }
}
