﻿using UnityEngine;


public class LapTimerManager : MonoBehaviour
{

    public TextMesh lap1Text;
    public TextMesh lap2Text;
    public TextMesh lap3Text;

    private float[] lapsTimers = new float[3];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Actualizamos los textos
        lap1Text.text = AssignText(0);
        lap2Text.text = AssignText(1);
        lap3Text.text = AssignText(2);
        // Se actualiza el timer en base a la vuelta actual
        if (GameStats.Win == false)
        {
            lapsTimers[GameStats.ActualLap] += Time.deltaTime;
        }
        
    }

    // Se calcula el tiempo, minutos y segundos en base al tiempo actual
    private string AssignText(int numberLap)
    {
        string minutes = Mathf.Floor(lapsTimers[numberLap] / 60).ToString("00");
        string seconds = (lapsTimers[numberLap] % 60).ToString("00");

        return (minutes + ":"+ seconds);
    }

    
}
