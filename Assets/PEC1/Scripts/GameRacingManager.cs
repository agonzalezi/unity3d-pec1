﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameRacingManager : MonoBehaviour
{

    public GhostManager actualGhost;
    public GhostPositionsManager manager;
    private bool finishedGame  =false;

    // Conjunto de coches
    public GameObject car;
    public GameObject ghostCar;
    public GameObject replayCar;

    // Camaras
    public GameObject cinematicCameras;
    public GameObject normalCameras;

    // Text Timer
    public GameObject timerTitle;
    public GameObject timerOne;
    public GameObject timerTwo;
    public GameObject timerThree;

    // Start is called before the first frame update
    void Start() {

        switch (GameStats.State)
        {
            // Se activa el coche principal y se inicia el juego en modo carrera. Se carga el fantasma en caso necesario
            case GameStats.GameMode.run:
                ShowTimers();
                GameStats.ResetGame();
                car.SetActive(true);
                // Si existe fantasma lo cargamos
                manager.LoadGhostPositions();
                actualGhost.LoadGhost();
                Debug.Log("LOAD GHOST");
                break;
            // Se activa el coche replay y se inicia el juego en modo replay normal
            case GameStats.GameMode.repetititon:
                ShowTimers();
                GameStats.ResetGame();
                actualGhost.LoadReplay();
                Debug.Log("REPLAY");
                replayCar.SetActive(true);
                break;
            // Se activa el coche replay y se inicia el juego en modo replay cinematico. Se desactiva la camara normal
            case GameStats.GameMode.cinematicrepetition:
                OccultTimers();
                GameStats.ResetGame();
                cinematicCameras.SetActive(true);
                normalCameras.SetActive(false);
                actualGhost.LoadReplay();
                replayCar.SetActive(true);
                break;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        // Si se ha cambiado de vuelta, guardamos el fantasma en caso necesario y lo ejecutamos
        if (GameStats.Win && !finishedGame)
        {
            Debug.Log("FINISH GAME");
            //Guardamos si es necesario el fantasma y el replay
            if(GameStats.State == GameStats.GameMode.run){
                actualGhost.SaveGhost();
                Debug.Log("SAVED GHOST");
                // guardar info de vuelta e ir al menu
                actualGhost.SaveReplay();
                Debug.Log("SAVED REPLAY");
            }
            
            finishedGame = true;

            // Volvemos al menu principal
            SceneManager.LoadScene("Menu");
        }
        else if (GameStats.State == GameStats.GameMode.run && GameStats.ChangedLap && !finishedGame)
        {
            // En caso de que el juego este en modo carrera, se guarda el fantasma y se carga el fantasma para que corra.
            Debug.Log("CHANGE LAP");
            GameStats.ChangedLap = false;
            actualGhost.SaveGhost();
            Debug.Log("SAVED GHOST");
            actualGhost.LoadGhost();
            Debug.Log("LOAD GHOST");
        }else if (GameStats.ChangedLap && !finishedGame)
        {
            // En caso de que el juego este en modo carrera, se guarda el fantasma y se carga el fantasma para que corra.
            Debug.Log("CHANGE LAP");
            GameStats.ChangedLap = false;
        }

    }

    // Ocultamos los timers en la repeticion cinematica
    private void OccultTimers()
    {
        timerTitle.SetActive(false);
        timerOne.SetActive(false);
        timerTwo.SetActive(false);
        timerThree.SetActive(false);
    }

    private void ShowTimers()
    {
        timerTitle.SetActive(true);
        timerOne.SetActive(true);
        timerTwo.SetActive(true);
        timerThree.SetActive(true);
    }
}
