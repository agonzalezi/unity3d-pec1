﻿using System.IO;
using UnityEngine;

public class GhostPositionsManager : MonoBehaviour
{
    //Nombre del fichero del fantasma desde donde se cargan las posiciones
    private string ghostDataFileName = "ghostInfo";
    //Nombre del fichero JSON donde se guardan las posiciones del fantasma
    private string ghostDataJSONFileName = "ghostInfo.json";

    //Nombre del fichero del replay desde donde se cargan las posiciones
    private string replayDataFileName = "replayInfo";
    //Nombre del fichero JSON donde se guardan las posiciones del fantasma
    private string replayDataJSONFileName = "replayInfo.json";

    //Colección de posiciones leído de ficheros
    public GhostPositions ghostPositionsSaved;

    public GhostPositions replayPositionsSaved;

    private void Start()
    {
       
    }

    // Carga las posiciones del fantasma de un fichero JSON a una estructura de datos
    public void LoadGhostPositions()
    {
        TextAsset targetFile = Resources.Load<TextAsset>(ghostDataFileName);
        if (targetFile!=null)
        {
            ghostPositionsSaved = JsonUtility.FromJson<GhostPositions>(targetFile.text);
            Debug.Log("Se han cargado " + ghostPositionsSaved.ghostPositionCollection.Count + " posiciones del fichero JSON");
        }

    }

    // Guarda las posiciones del fantasma en un fichero JSON para poder usarlo
    public void SaveGhostPositions(GhostPositions ghostNewPositions)
    {
        TextAsset targetFile = Resources.Load<TextAsset>(ghostDataFileName);
        string jsonGhostPosition = JsonUtility.ToJson(ghostNewPositions);
        File.WriteAllText(Application.dataPath+"/Resources/"+ ghostDataJSONFileName, jsonGhostPosition);
        ghostPositionsSaved = ghostNewPositions;
    }

    // Carga las posiciones del replay de un fichero JSON a una estructura de datos
    public void LoadReplayPositions()
    {
        TextAsset targetFile = Resources.Load<TextAsset>(replayDataFileName);
        if (targetFile != null)
        {
            replayPositionsSaved = JsonUtility.FromJson<GhostPositions>(targetFile.text);
            Debug.Log("Se han cargado " + replayPositionsSaved.ghostPositionCollection.Count + " posiciones del fichero JSON");
        }

    }

    // Guarda las posiciones del replay en un fichero JSON para poder usarlo
    public void SaveReplayPositions(GhostPositions ghostNewPositions)
    {
        TextAsset targetFile = Resources.Load<TextAsset>(replayDataFileName);
        string jsonReplayPosition = JsonUtility.ToJson(ghostNewPositions);
        File.WriteAllText(Application.dataPath + "/Resources/" + replayDataJSONFileName, jsonReplayPosition);
        replayPositionsSaved = ghostNewPositions;
    }
}
