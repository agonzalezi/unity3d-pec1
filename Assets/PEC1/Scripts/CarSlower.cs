﻿using UnityEngine;

public class CarSlower : MonoBehaviour
{
    public WheelCollider wheelFrontRight;
    public WheelCollider wheelFrontLeft;
    public WheelCollider wheelRearRight;
    public WheelCollider wheelRearLeft;

    private float slowFactor = 15f;
    private float originalFactor = 0.25f;

    // Con un collider inferior, se detecta cuando esta tocando terreno que no es carretera para cambiar el factor de amortiguacion que provoca una lentitud
    void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.CompareTag("Road"))
        {
            wheelFrontRight.wheelDampingRate = slowFactor;
            wheelFrontLeft.wheelDampingRate = slowFactor;
            wheelRearRight.wheelDampingRate = slowFactor;
            wheelRearLeft.wheelDampingRate = slowFactor;
        }
        else
        {
            wheelFrontRight.wheelDampingRate = originalFactor;
            wheelFrontLeft.wheelDampingRate = originalFactor;
            wheelRearRight.wheelDampingRate = originalFactor;
            wheelRearLeft.wheelDampingRate = originalFactor;
        }

    }
}
