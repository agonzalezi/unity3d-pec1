﻿using System;
using System.Collections.Generic;

[Serializable]
public class GhostPositions
{
    // Tiempo total de la vuelta del fantasma
    public float totalTime;

    //Lista de posiciones del fantasma, cargados desde JSON.
    public List<GhostPosition> ghostPositionCollection;


}
