﻿using System;

[Serializable]
public class GhostPosition 
{
    // Objeto que representa las posiciones y rotaciones del coche
    public float positionx;
    public float positiony;
    public float positionz;

    public float rotationx;
    public float rotationy;
    public float rotationz;
    public float rotationw;
}
