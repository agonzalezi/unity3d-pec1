# PAC 1 - Alex Racing

El objetivo de esta práctica es realizar un juego de carreras, en el cual puedes correr con un fantasma creado con la ruta del
jugador en una vuelta anterior. Las posiciones del fantasma se guardan y se cargan desde un fichero en la carpeta Resources.
El fantasma se carga en cada vuelta en caso de que ya se haya dado una vuelta.
Adicionalmente se puede ver la ultima repeticion con camara normal o camara cinematica.

## Elementos principales

El juego consta de dos escenas, una para el menu principal y otra para el juego.

### Menu

- Scripts:
	- MenuManager: Permite iniciar el juego en los diferentes modos:
		- Carrera
		- Repeticion normal
		- Repeticion cinematica

- Elementos gráficos:
	- START RUN: Inicia el juego en modo carrera
	- VIEW REPETITION: Inicia el juego en modo repeticion en base al tipo de repeticion seleccionado.
	- Slider tipo de repeticion: Selecciona el tipo de repeticion a visualizar. Normal o cinematica.
	- CHECK: Permite verificar si el fichero JSON de repeticion existe, para poder verla y activar el boton de ver la repeticion.
	- EXIT: Permite salir del juego


### Game

- Scripts:
	- GameRacingManager: Es el encargado de configurar el juego en base a la seleccion hecha en el menu principal:
		- Carrera: activa el coche que el jugador puede controlar y la camara que sigue al jugador.
		- Repeticion normal: activa el coche replica como el del jugador, carga las posiciones del fichero y la camara que sigue al coche.
		- Repeticion cinematica: activa el coche replica como el del jugador, carga las posiciones del fichero y el conjunto de camaras cinematicas.
	- CarSlower: En base al choque del collider inferior del coche, copia del body pero en la parte inferior, se realentiza el coche si esta sobre el terreno, fuera de la carretera.
	- CheckpointCollisionManager: Detecta la colision en el checkpoint e invoca al script padre.
	- CheckpointsManager: Gestiona el conjunto de checkpoints, desactivando cuando se ha colisionado con el y activando el siguiente.
	- GhostManager: Clase encargada de guardar las posiciones tanto del fantasma como de la repeticion. En caso de que se haya mejorado el tiempo de la vuelta, se encargara de actualizar el fantasma.
	- GhostPosition: Objeto de datos que permite guardar la posicion y rotacion del coche en cada momento.
	- GhostPositions: Objeto de datos que contiene el tiempo de la vuelta y las posiciones y rotaciones del coche. Se usa tanto para el fantasma como para la repeticion.
	- GhostPositionsManager: Es la clase encargada de guardar y cargar desde los ficheros JSON tanto la informacion del fantasma como de la repeticion.
	- LapTimerManager: Muestra por pantalla el tiempo de cada vuelta.
	- MenuManager: Permite volver al menu principal o salir del juego.
	- PauseManager: Si se pulsa la tecla espacio, permite activar el menu de pausa para volver al menu o salir del juego.

- Elementos gráficos:
	- Coche: Puede ser movido por el jugador con las teclas de direccion.
	- Menú de pausa: es un menú con dos botones que aparece al pulsar la tecla Espacio. Permite ir al menu principal o salir del juego.
	- Carretera: Permite correr con el coche.
	- Terreno: Permite correr con el coche pero a menor velocidad que si se corre por la carretera.
	- Checkpoint: Deben ser activados en orden para poder finalizar la carrera.
	- Timers: En la parte superior derecha de la pantalla se muestran los tiempos de cada vuelta.
	- Fantasma: En base al recorrido que ha realizado el jugador en cada vuelta, se guarda su informacion en un fichero y se activa para seguir el recorrido realizado por el jugador en la vuelta anterior.
	- Coche replay: Coche identico al coche del jugador pero sigue un trazado con las posiciones que ha realizado el jugador en la ultima carrera.

### Clases adicionales

- GameStats: Clase estática, que sirve para guardar el modo de juego y sus valores; y asi poder usar esta información en la escena. Contabiliza las vueltas para determinar el final de la carrera.



## Elementos adicionales

- Materials: Materiales usados.

- Music: Musica y sonidos del juego utilizados.

- Resources: Carpeta donde se guardan los ficheros del fantasma y de la ultima repeticion.

## IMPORTANTE

Dentro de cada script están comentados tanto las variables como los métodos, para entenderlo mejor.

## Changelog
* Version 0.1
Terreno
Checkpoint
Coche
* Version 0.2
Timers
* Version 0.3
Menu
* Version 0.4
Guardado del fantasma
* Version 0.5
Carga del fantasma
* Version 0.6
Lentitud en el coche al salir de la carretera
* Version 0.65
Bugfix
* Version 0.7
Repeticion normal
* Version 0.8
Repeticion cinematica
* Version 0.9
Menu pausa
Bugfix
* Version 0.95
Bugfix
* Version 0.98
Bugfix
* Version 1.0
Entrega final
