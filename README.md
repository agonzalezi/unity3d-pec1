# PAC 1 - Alex Racing

El objetivo de esta práctica es realizar un juego de carreras, en el cual puedes correr con un fantasma creado con la ruta del
jugador en una vuelta anterior. Las posiciones del fantasma se guardan y se cargan desde un fichero en la carpeta Resources.
El fantasma se carga en cada vuelta en caso de que ya se haya dado una vuelta.
Adicionalmente se puede ver la ultima repeticion con camara normal o camara cinematica.

## IMPORTANTE
Tal y como se comenta en el video, exite un bug al realizar una build del juego. Guarda correctamente en ficheros JSON tanto el fantasma como la repeticion.
Pero no consigue cargarlos, me da la sensacion que es algun problema con la cache, dado que parece que los coge de la ruta original donde esta el codigo o estan en cache o en el paquete creado.
En el editor de unity funciona correctamente.
Parece ser algun tipo de configuracion que no encuentro al hacer el build. He tenido que volver a realizar un build para que detectara los ficheros.

## Descripción del juego

### Objetivo

El objetivo principal del juego es completar una carrera que consta 3 vueltas.

### Participantes

En el juego participa un único jugador que controlará al coche, el fantasma será controlado por la IA.

### Controles
Flechas de dirección: permiten mover al coche.
Barra espaciadora: aparece el menu de pausa que permite salir del juego o ir al menu principal.

### Funcionamiento

En el menú superior derecha aparecen los siguientes indicadores:
- Si el menu de pausa está activo, aparecen 2 botones para salir del juego o ir al menu.
- Tiempo de las vueltas. El jugador puede ver el tiempo que ha tardado en realizar cada vuelta.

En el juego, aparecen diferentes elementos:
- Menu principal: Permite seleccionar si se quiere jugar o ver la repeticion. Consta de los siguientes elementos:
	- Start race: Permite correr una carrera de dos vueltas.
	- View repetition: Permite ver la repeticion de la ultima carrera.
	- Selector de repeticion: Slider que permite seleccionar el tipo de repeticion que se quiere ver, normal o cinematica.
	- Check: Permite comprobar si se ha creado el fichero de repeticion para poderla ver.
	- Exit: Permite salir del juego.
- Menu de pausa: Aparece al pulsar la barra espaciadora. Permite salir del juego o ir al menu principal.
- Carretera: Donde el coche puede correr.
- Terreno: Donde el coche puede correr pero a menos velocidad 
- Coche: El jugador puede moverlo con las flechas de dirección.
- Coche fantasma: Cuando el jugador ha realizado una vuelta, se guardan sus posiciones y se carga un coche fantasma que realiza el mismo recorrido.
- Checkpoints: Se activan al pasar el coche y sirven para verificar que el jugador ha hecho todo el recorrido.

### Consideraciones

- En el caso de que el coche salga de la pista, se reducirá su velocidad.
- Una vez el coche vuelva a la carretera ya podrá correr de forma normal.
- El jugador debe de pasar por TODOS los checkpoints, si el jugador se salta alguno, deberá regresar para activarlo.
- Los checkpoints se activan por orden, si no se ha pasado por uno, no se activará el siguiente.
- La información del fantasma y de la repetición se guarda en ficheros JSON.

### Acciones implementadas

- Correr: El juego permite correr en una carrera de 3 vueltas, en cada vuelta aparecera un fantasma que seguirá el trazado que ha realizado el jugador en la vuelta anterior.
- Fantasma: Un coche transparente que realiza la carrera que el jugador ha realizado anteriormente.
- Lentitud al no ir por carretera: Hace que el coche vaya mas lento al no ir por carretera.
- Sistema de checkpoints: Un conjunto de checkpoints que verifican que el jugador realice todo el trazado de la carrera y no haga trampas.
- Guardado de la informacion en JSON: Se guarda la informacion tanto del fantasma como de la repeticion en unos ficheros JSON.
- Repeticion normal: Permite ver la repeticion de la ultima carrera con la misma camara de la carrera.
- Repeticion cinematica: Permite ver la repeticion de la ultima carrera con un conjunto de camaras que van intercambiandose.
- Reinicio del juego: Permite salir al menu para poder reiniciar el juego, pulsando la barra espaciadora.
- Menu principal: Permite seleccionar que accion quiere hacer el jugador, jugar o ver la repeticion.

### IMPORTANTE

El vídeo de gameplay comentado está en la carpeta:  Assets/PEC1/Gameplay-PAC1.mp4
Adicionalmente esta subido a Youtube en la url: https://www.youtube.com/watch?v=QNC2PA7SMAo&feature=youtu.be

El Readme adicional explicativo de las clases de la practica está en: Assets/PEC1/README.md
	
### Próximos pasos

- Selector de circuitos: Crear un menu para seleccionar el circuito. Con un conjunto de checkpoints propio y una carretera.
- Selector de coches: Poder seleccionar el tipo de coche.
- Vista en primera persona: Poder cambiar entre vista en tercera persona y primera persona, donde se veria una simulacion de un salpicadero de coche.